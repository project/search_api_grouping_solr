Search API Grouping Solr

One of the biggest issues with Search API is the ability to break out data within Search API by fields. The following module leverages the Search API Grouping module to help denormalize fields and break them into their own documents. Then when using views allows for the field to be used in the entity

At the moment Search API Grouping does not have a stable version for Drupal 10+. There are patches in the queue that are currently.

Current Issue Outstanding:

- Automated Drupal 10 compatibility fixes: https://www.drupal.org/project/search_api_grouping/issues/3289516
- Error "Call to a member function getValues() on null" preventing data to be indexed when Denormalization is enabled: https://www.drupal.org/project/search_api_grouping/issues/3319197
- PHP notices in Denormalization processor: https://www.drupal.org/project/search_api_grouping/issues/3246441

Installing

Composer Lenient is required at the moment as Search API Grouping in order to move forward with installing the module.

composer require mglaman/composer-drupal-lenient

composer config --merge --json extra.drupal-lenient.allowed-list '["drupal/search_api_grouping"]'

composer config --merge --json extra.patches.drupal/search_api_grouping '{"Issue #3289516: Drupal 10 Comp": "https://www.drupal.org/files/issues/2022-06-16/search_api_grouping.1.x-dev.rector.patch"}'

composer config --merge --json extra.patches.drupal/search_api_grouping '{"Issue #3319197: Call to member function getValues": "https://www.drupal.org/files/issues/2022-11-04/3319197-getvalue-2.patch"}'

composer config --merge --json extra.patches.drupal/search_api_grouping '{"Issue #3275014: element_validate_integer_positive not found": "https://www.drupal.org/files/issues/2022-04-13/element_validate_integer_positive-not-found-3275014-1.patch"}'

composer config --merge --json extra.patches.drupal/search_api_grouping '{"Issue #3360720: Warning foreach checkboxes": "https://www.drupal.org/files/issues/2023-05-16/3360720-warning-foreach-argument.patch"}'

composer config --merge --json extra.patches.drupal/search_api_grouping '{"Issue #3423336: getDenormalizationFields does not return field names": "https://git.drupalcode.org/project/search_api_grouping/-/merge_requests/4.diff"}'

composer require 'drupal/search_api_grouping:1.x-dev' drupal/search_api_grouping_solr

After installing the modules via composer then enable the module by going to Extend then selecting the module.

Once enabled go to the Processors for the desired Index and enable the "Denormalization" and "Denormalization - Altering" plugins.

Scroll to the bottom and select the fields that should be broken out. Under the Denormalization settings.

Save.

After, the index will need to reindex the entities.

When that is done creating a view now will create separate rows for each of the data points provided and allow sorting to properly happen.

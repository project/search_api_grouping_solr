<?php

namespace Drupal\search_api_grouping_solr\Plugin\search_api\processor;

use Drupal\search_api\IndexInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\Query\ResultSetInterface;
use Drupal\search_api\SearchApiException;
use Drupal\search_api_solr\SolrBackendInterface;

/**
 * Plugin for Altering the denormalized documents in solr.
 *
 * @SearchApiProcessor(
 *   id = "denormalization_altering",
 *   label = @Translation("Denormalization - Altering"),
 *   description = @Translation("This processor works with altering the denormalized documents with the provided document."),
 *   stages = {
 *     "preprocess_query" = -10,
 *     "postprocess_query" = -10,
 *   },
 * )
 */
class DenormalizeAltering extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public static function supportsIndex(IndexInterface $index) {
    // Check to see if Solr is enabled as currently the only supported backend.
    return ($index->getServerInstance()->getBackend() instanceof SolrBackendInterface);
  }

  /**
   * Checks to see if denormalization Plugin is enabled.
   *
   * @param IndexInterface $index
   *   The index checking against.
   *
   * @return bool
   *   TRUE if plugin is enabled, FALSE if not.
   */
  public function isDenormalizationEnabled(IndexInterface $index): bool {
    // @todo figure out a way to do this as a plugin dependency.
    try {
      $index->getProcessor('denormalization');
    }
    catch (SearchApiException $exception) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function preprocessSearchQuery(QueryInterface $query) {
    // If plugin isn't enabled skip.
    if (!$this->isDenormalizationEnabled($query->getIndex())) {
      return;
    }

    $backend = $query->getIndex()->getServerInstance()->getBackend();
    $backendSettings = $backend->getConfiguration();
    $backendSettings["retrieve_data"] = true;
    $backend->setConfiguration($backendSettings);

    /** @var \Drupal\search_api_grouping\Plugin\search_api\processor\Denormalize $denormalization */
    $denormalization = $query->getIndex()->getProcessor('denormalization');

    $query->setOption('search_api_retrieved_field_values', array_merge(
      $query->getOption('search_api_retrieved_field_values'),
      array_keys($denormalization->getDenormalizationFields())
    ));
    parent::preprocessSearchQuery($query);
  }

  /**
   * {@inheritdoc}
   */
  public function postprocessSearchResults(ResultSetInterface $results) {
    // Get the index.
    $index = $results->getQuery()->getIndex();

    // If plugin isn't enabled skip.
    if (!$this->isDenormalizationEnabled($index)) {
      return;
    }

    /** @var \Drupal\search_api_grouping\Plugin\search_api\processor\Denormalize $denormalization */
    $denormalization = $index->getProcessor('denormalization');

    // Return a list of all data.docs returned from solr.
    $solarResponse = $results->getAllExtraData()['search_api_solr_response'];
    $docs = $solarResponse['response']['docs'] ?? [];

    // List of items that were returned as part of search results.
    $oldResultItems = $results->getResultItems();

    // Store original values from entities here.
    $originalValues = [];

    // Store values in running array that are unaltered.
    foreach ($oldResultItems AS $oldResultItem) {
      foreach (array_keys($denormalization->getDenormalizationFields()) as $field_id) {
        /** @var \Drupal\Core\Entity\EntityInterface $entity */
        $entity = $oldResultItem->getOriginalObject()->getEntity();
        if (method_exists($entity, 'get')) {
          try {
            $originalValues[$oldResultItem->getId()][$field_id] = $entity->get($field_id)->getValue();
          }
          catch (\InvalidArgumentException $exception) {
            // Skip as the entity doesn't have the field.
          }
        }
      }
    }

    // Get a list of all the solr fields and their index mapping.
    $solrFields = $index->getServerInstance()->getBackend()->getSolrFieldNames($index);

    // Keep track of the new items to add to the search results.
    $newResultItems = [];

    // Loop through all the documents returned from solr.
    foreach ($docs as $doc) {
      // Clone the old result item returned.
      $newItem = clone($oldResultItems[$doc[$solrFields['search_api_id']]]);

      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      // Clone the entity as not to disturb the original version.
      $entity = clone($newItem->getOriginalObject()->getEntity());
      // Clone the original object to modify a new version of it.
      $object = clone($newItem->getOriginalObject());
      // Set the cloned entity on the cloned object.
      $object->setValue($entity);
      // Set the cloned object back to the new item.
      $newItem->setOriginalObject($object);

      // Loop through the denormalized fields and alter the entities.
      foreach (array_keys($denormalization->getDenormalizationFields()) as $field_id) {
        // If the document doesn't have the field set skip.
        if (!isset($doc[$solrFields[$field_id]])) {
          continue;
        }

        // Change to work with all types of fields.
        $fieldInfo = $index->getField($field_id);

        // Set to the first item in the list.
        $value = [$doc[$solrFields[$field_id]][0]];

        // Alter value just based on date fields.
        if ($fieldInfo->getType() === 'date') {
          // Change the value to a timestamp as it is returned as YYYY-mm-ddTH:i:sZ.
          $value[0] = strtotime($value[0]) ?? '';
        }

        // Set field value in the result item.
        $newItem->getField($field_id)->setValues($value);

        // Get the original values.
        $fullValues = $originalValues[$newItem->getId()][$field_id] ?? [];
        // Get the first item off the indexed value.
        $indexedValue = $newItem->getField($field_id)->getValues()[0];

        // Currently this is only necessary
        $valueKey = 'value';

        // Try to find the index for the specific value that was indexed.
        if (($valueIndex = array_search($indexedValue, array_column($fullValues, $valueKey))) !== FALSE) {
          // Set the entity value for the field to the index value.
          $newValue = [$fullValues[$valueIndex]];
          $entity->set($field_id, $newValue);
        }
      }

      // Add modified item to the new result array with the value key.
      $newResultItems[] = $newItem;
    }

    // Set the results to the new items array previously created.
    $results->setResultItems($newResultItems);
  }

}
